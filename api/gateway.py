# -*- coding: utf-8 -*-
#
# Created: 16/11/2018 10:50:32
# Author: Basask (basask@gmail.com)

import os

from nameko.standalone.rpc import ClusterRpcProxy
from sanic import Sanic
from sanic.response import json

app = Sanic()
CONFIG = {
    'AMQP_URI': os.environ.get('RABBIT_URL', 'amqp://guest:guest@127.0.0.1')
}


@app.route('/report', methods=['POST'])
def report(request):
    key = request.json.get('key')

    with ClusterRpcProxy(CONFIG) as rpc:

        # TODO: Consume RPC services in an asyncronous way
        response_a = rpc.consumer_a.query(key)
        response_b = rpc.consumer_b.query(key)
        response_c = rpc.consumer_c.query(key)

        response = {
            **response_a,
            **response_b,
            **response_c,
        }

        return json(response)


app.run(host='0.0.0.0', port=5000)
