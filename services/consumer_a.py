# -*- coding: utf-8 -*-
#
# Created: 16/11/2018 11:46:21
# Author: Basask (basask@gmail.com)

import os

import requests
from nameko.rpc import RpcProxy, rpc


class ConsumerServiceA(object):
    name = "consumer_a"

    TARGET = os.environ.get('EXPDATA_URL', 'http://127.0.0.1:8881')

    storage = RpcProxy('mongo_storage')

    @rpc
    def query(self, key):
        query = {'cpf': key}
        cached = self.storage.get_entry('user', query, db='ext-a')

        if cached:
            return cached

        url = '{}/{}'.format(self.TARGET, key)
        response = requests.get(url)

        data = response.json()
        self.storage.add_entry('user', data, db='ext-a')
        return data
