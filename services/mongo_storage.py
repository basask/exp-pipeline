# -*- coding: utf-8 -*-
#
# Created: 16/11/2018 12:12:04
# Author: Basask (basask@gmail.com)

import json
import os

from bson.json_util import dumps
from nameko.rpc import rpc
from pymongo import MongoClient

CLIENT = MongoClient(
    os.environ.get('MONGO_URL', 'mongodb://127.0.0.1:27017/')
)


class MongoStorage(object):
    name='mongo_storage'

    def get_database(self, db_name):
        database=CLIENT[db_name]
        db_name=db_name or 'default'
        return database

    @rpc
    def get_entry(self, collection, query, db=None):
        print('MongoStorage::get_entry')
        db=self.get_database(db)
        collection=db[collection]
        results=collection.find_one(query)
        # TODO: Find a better way to make pyMongo cursor serializable
        return json.loads(dumps(results))

    @rpc
    def add_entry(self, collection, entry, db=None):
        print('MongoStorage::add_entry')
        db=self.get_database(db)
        collection=db[collection]
        collection.insert_one(entry)
