# App schema

This app is composed of one API Gateway (Report) and few services(Consumer A, Consumer B, ...) to consume and store data from external DBs (DB A, DB B, DB C)

These services are connected through Rest APIS or RPC as shown bellow.
![App schema](./images/schema.png)

# Deploy solution:

## Data generator

One project was made only to generate data for this one.

The project named Apid-data-faker is hosted on https://github.com/basask/api-data-faker


Please follow the building instructions on https://github.com/basask/api-data-faker#docker

## Docker instances

This project depends on MongoDb, RabbitMQ and API-data-faker.

To run these apps follow the instructions bellow.

```bash
# Rabbitmq docker service
docker run -p 5672:5672 --hostname exp-rabbit-hub rabbitmq:3

# MongoDB service
docker run -p 27017:27017 mongo:latest

# Part of API-data-faker project
docker run -p 8881:8881 -p 8882:8882 -p 8883:8883 api-data-faker
```

## Running services

```bash
# At the root of application (exp-pipeline) run
nameko run services
```

## Running the api

```bash
# At the root of application (exp-pipeline) run
python ./api/gateway.py
```

## Test

Make a http request on http://localhost:5000/ like the one bellow:
```bash
curl -XPOST -H "Content-Type: application/json" -d'{"key": "047.644.222-45"}' http://localhost:5000/
```

Where the key is the brazilian CPF document number.

# Work on progress

However, due to a lack of time it's not ready yet.

## Deploy

Some effort was made in order to deliver a better deploy experience through docker compose.

## Services

At this time only the mongoDb storage service is coded. The ideia is to create 2 more additional services (Redis and ElasticSearch) to be used with data from Database B and C respectely.


# Used technologies

- Nameko (Microservices framework): https://www.nameko.io
- RabbitMq (Massage broker): https://www.rabbitmq.com
- MongoDb (NoSQL database): https://www.mongodb.com
- Sanic (Web microframework): https://sanicframework.org
- Docker (Container automation): https://www.docker.com
- Apid Data Faker (Generate fake data): https://github.com/basask/api-data-faker
